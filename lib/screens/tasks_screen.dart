import 'package:flutter/material.dart';
import 'package:todoey/models/task.dart';
import 'package:todoey/widgets/tasks_list.dart';
import 'add_task_screen.dart';

class TasksScreen extends StatefulWidget {
  @override
  _TasksScreenState createState() => _TasksScreenState();
}

class _TasksScreenState extends State<TasksScreen> {
  List<Task> tasksList = [];
  int tasksDone = 0;
  int tasksLeft = 0;
  void checkTask(int index, bool checked) {
    setState(() {
      tasksList[index].isChecked = checked;
      print(checked);
      if (checked == true) {
        tasksDone++;
        tasksLeft--;
      } else {
        tasksDone--;
        tasksLeft++;
      }
    });
  }

  void addTask(
    String taskName,
  ) {
    setState(() {
      tasksList.add(
        Task(
          taskText: taskName,
        ),
      );
      tasksLeft++;
    });
  }

  Text displayText() {
    if (tasksList.length == 0) {
      return (Text(
        '☀ Nimic \n',
        style: TextStyle(
          fontSize: 50,
          color: Colors.white70,
          fontWeight: FontWeight.bold,
        ),
      ));
    } else {
      if (tasksLeft == 0) {
        return Text(
          '☀ Lista completa! ☀',
          style: TextStyle(
            fontSize: 70,
            color: Colors.white70,
            fontWeight: FontWeight.bold,
          ),
        );
      } else if (tasksList.length == 1) {
        return Text(
          '1 lucru\n$tasksDone făcute \n$tasksLeft rămase',
          style: TextStyle(
            fontSize: 25,
            color: Colors.white70,
            fontWeight: FontWeight.bold,
          ),
        );
      } else {
        return Text(
          '${tasksList.length} lucruri\n$tasksDone făcute \n$tasksLeft rămase',
          style: TextStyle(
            fontSize: 25,
            color: Colors.white70,
            fontWeight: FontWeight.bold,
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlueAccent,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
            context: context,
            builder: (context) => SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: AddTask(
                  addTask: addTask,
                ),
              ),
            ),
          );
        },
        child: Icon(
          Icons.add,
          size: 40,
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 60,
              vertical: 0,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    top: 100,
                  ),
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 40,
                    child: Icon(
                      Icons.list,
                      color: Colors.lightBlue,
                      size: 50,
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                Text(
                  'De făcut',
                  style: TextStyle(
                    fontSize: 60,
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    decoration: TextDecoration.underline,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                displayText(),
              ],
            ),
          ),
          SizedBox(
            height: 60,
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(60.0),
                child: TasksList(
                  tasksList: tasksList,
                  onPress: checkTask,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
