import 'package:flutter/material.dart';
import 'package:todoey/models/task.dart';
import 'task_tile.dart';

class TasksList extends StatelessWidget {
  final List<Task> tasksList;
  final Function onPress;
  TasksList({this.tasksList, this.onPress});
  @override
  Widget build(BuildContext context) {
    print(onPress);
    return ListView.builder(
      itemBuilder: (context, index) {
        bool isChecked = tasksList[index].isChecked;
        return TaskTile(
          taskTitle: tasksList[index].taskText,
          isChecked: tasksList[index].isChecked,
          index: index,
          onPress: (index, isChecked) {
            onPress(index, isChecked);
          },
        );
      },
      itemCount: tasksList.length,
    );
  }
}
