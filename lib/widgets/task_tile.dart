import 'package:flutter/material.dart';

class TaskTile extends StatelessWidget {
  final String taskTitle;
  final int index;
  final bool isChecked;
  final Function onPress;
  TaskTile({this.taskTitle, this.isChecked, this.onPress, this.index});
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        '$taskTitle',
        style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.w700,
          decoration: isChecked ? TextDecoration.lineThrough : null,
        ),
      ),
      trailing: Checkbox(
        value: isChecked,
        onChanged: (value) {
          print(index);
          onPress(index, value);
        },
      ),
    );
  }
}
